package com.bbva.reod.dto.dinamic;

import java.util.HashMap;
import java.util.Map;

import com.bbva.apx.dto.AbstractDTO;

public class AlternativasDTO extends AbstractDTO{
	
	private static final long serialVersionUID = -5138147277273205817L;
	
	private String producto;
	private String subproducto;
	private String plazo;
	private String tasa; 
	private String importeAlternativa;
	private String pagoMensual;
	private String descripcionSubproducto;
	private String impMaximo;
	private String impMinimo;
	
	public final String getProducto() {
		return producto;
	}
	public final void setProducto(String producto) {
		this.producto = producto;
	}
	public final String getSubproducto() {
		return subproducto;
	}
	public final void setSubproducto(String subproducto) {
		this.subproducto = subproducto;
	}
	public final String getPlazo() {
		return plazo;
	}
	public final void setPlazo(String plazo) {
		this.plazo = plazo;
	}
	public final String getTasa() {
		return tasa;
	}
	public final void setTasa(String tasa) {
		this.tasa = tasa;
	}
	public final String getImporteAlternativa() {
		return importeAlternativa;
	}
	public final void setImporteAlternativa(String importeAlternativa) {
		this.importeAlternativa = importeAlternativa;
	}
	public final String getPagoMensual() {
		return pagoMensual;
	}
	public final void setPagoMensual(String pagoMensual) {
		this.pagoMensual = pagoMensual;
	}
	public final String getDescripcionSubproducto() {
		return descripcionSubproducto;
	}
	public final void setDescripcionSubproducto(String descripcionSubproducto) {
		this.descripcionSubproducto = descripcionSubproducto;
	}
	public final String getImpMaximo() {
		return impMaximo;
	}
	public final void setImpMaximo(String impMaximo) {
		this.impMaximo = impMaximo;
	}
	public final String getImpMinimo() {
		return impMinimo;
	}
	public final void setImpMinimo(String impMinimo) {
		this.impMinimo = impMinimo;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcionSubproducto == null) ? 0 : descripcionSubproducto.hashCode());
		result = prime * result + ((impMaximo == null) ? 0 : impMaximo.hashCode());
		result = prime * result + ((impMinimo == null) ? 0 : impMinimo.hashCode());
		result = prime * result + ((importeAlternativa == null) ? 0 : importeAlternativa.hashCode());
		result = prime * result + ((pagoMensual == null) ? 0 : pagoMensual.hashCode());
		result = prime * result + ((plazo == null) ? 0 : plazo.hashCode());
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + ((subproducto == null) ? 0 : subproducto.hashCode());
		result = prime * result + ((tasa == null) ? 0 : tasa.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlternativasDTO other = (AlternativasDTO) obj;
		if (descripcionSubproducto == null) {
			if (other.descripcionSubproducto != null)
				return false;
		} else if (!descripcionSubproducto.equals(other.descripcionSubproducto))
			return false;
		if (impMaximo == null) {
			if (other.impMaximo != null)
				return false;
		} else if (!impMaximo.equals(other.impMaximo))
			return false;
		if (impMinimo == null) {
			if (other.impMinimo != null)
				return false;
		} else if (!impMinimo.equals(other.impMinimo))
			return false;
		if (importeAlternativa == null) {
			if (other.importeAlternativa != null)
				return false;
		} else if (!importeAlternativa.equals(other.importeAlternativa))
			return false;
		if (pagoMensual == null) {
			if (other.pagoMensual != null)
				return false;
		} else if (!pagoMensual.equals(other.pagoMensual))
			return false;
		if (plazo == null) {
			if (other.plazo != null)
				return false;
		} else if (!plazo.equals(other.plazo))
			return false;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (subproducto == null) {
			if (other.subproducto != null)
				return false;
		} else if (!subproducto.equals(other.subproducto))
			return false;
		if (tasa == null) {
			if (other.tasa != null)
				return false;
		} else if (!tasa.equals(other.tasa))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "AlternativasDTO [producto=" + producto + ", subproducto=" + subproducto + ", plazo=" + plazo + ", tasa="
				+ tasa + ", importeAlternativa=" + importeAlternativa + ", pagoMensual=" + pagoMensual
				+ ", descripcionSubproducto=" + descripcionSubproducto + ", impMaximo=" + impMaximo + ", impMinimo="
				+ impMinimo + "]";
	}
	
	
	public Map<String,Object> toMap() {
	    Map<String,Object> objectMap = new HashMap<>();
	    objectMap.put("producto",this.producto);
	    objectMap.put("subproducto",this.subproducto);
	    objectMap.put("plazo",this.plazo);
	    objectMap.put("tasa",this.tasa);
	    objectMap.put("importeAlternativa",this.importeAlternativa);
	    objectMap.put("pagoMensual",this.pagoMensual);
	    objectMap.put("descripcionSubproducto",this.descripcionSubproducto);
	    objectMap.put("impMaximo",this.impMaximo);
	    objectMap.put("impMinimo",this.impMinimo);
	    return objectMap;
	}

}
