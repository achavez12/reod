package com.bbva.reod.lib.r001.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.db.NoResultException;
import com.bbva.reod.dto.dinamic.AlterReq;
import com.bbva.reod.dto.dinamic.AlternativasDTO;
import com.bbva.reod.dto.dinamic.CreditosResponse;
import com.bbva.reod.dto.dinamic.CreditosResponseBuilder;
import com.bbva.reod.dto.dinamic.LoginReq;
import com.bbva.reod.lib.r001.REODR001;

public class REODR001Impl extends REODR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(REODR001.class);

	@Override
	public String executelogin(LoginReq req) {
		String result = "";
		try{
			LOGGER.info("lib req: {}, {}",req.getClient(),req.getPassword());
			Map<String, Object> pkMap = jdbcUtils.queryForMap("SELECT_CLIENTE", req.toMap());
			LOGGER.info("lib map: {}",pkMap.get("client"));
			if(pkMap.get("client").equals(req.getClient())){
				result = "1";
			}
		}catch(NoResultException e){
			LOGGER.info("Sin Resultado: {}, {}",e.getAdviceCode(),e.getMessage());
			this.addAdvice("01319001");
			result="0";
		}
		return result;
	}

	@Override
	public List<CreditosResponse> executeCreditos(String client) {
		LOGGER.info("lib client: {}",client);
		List<CreditosResponse> list = new ArrayList<>();
		try{
	    	List<Map<String, Object>> rows = jdbcUtils
					.queryForList("SELECT_CREDITOS", client);
	    	for (Map<String, Object> rowMap : rows) {
	    		CreditosResponse builder = CreditosResponseBuilder.aCreditosResponseBuilder()
	    			.withCliente(rowMap.get("CD_CLIENTE").toString())
	    			.withFecha_contratacion(rowMap.get("FE_CONTRATACION").toString())
	    			.withNumContrato(rowMap.get("CD_CONTRATO").toString())
	    			.withMensualidad(rowMap.get("MENSUALIDAD").toString())
	    			.withMonto(rowMap.get("MONTO").toString())
	    			.withProducto(rowMap.get("CD_PRODUCTO").toString())
	    			.withStatus(rowMap.get("STATUS").toString())
	    			.build();
	    		list.add(builder);
		    }
	    }catch(NoResultException e){
	    	LOGGER.info("Sin Resultado: {}, {}",e.getAdviceCode(),e.getMessage());
			this.addAdvice("01319001");
		}
	return list;
	}

	@Override
	public String executeCancelarCredito(String contrato) {
		String result="0";
		try{
			int rowsUpdated = jdbcUtils.update("UPDATE_C", contrato);
			if(rowsUpdated==1){
				result="1";
			}
	    }catch(NoResultException e){
	    	LOGGER.info("Error en cancelación: {}, {}",e.getAdviceCode(),e.getMessage());
		}
		return result;
	}

	@Override
	public String executeContratacion(CreditosResponse req) {
		String result="0";
		try{
			Map<String,Object> var = new HashMap<String, Object>();
			var.put("numcontrato", req.getNumcontrato());
			var.put("cliente", req.getCliente());
			var.put("producto", req.getProducto());
			var.put("fecha_contratacion", req.getFecha_contratacion());
			var.put("monto", req.getMonto());
			var.put("mensualidad", req.getMensualidad());
			int res= jdbcUtils.update("INSERT_INTO", var);
			result="1";
	    }catch(NoResultException e){
	    	LOGGER.info("Error en Contratacion: {}, {}",e.getAdviceCode(),e.getMessage());
		}
		return result;
	}

	@Override
	public List<AlternativasDTO> executeAlternativas(AlterReq req) {
		List<AlternativasDTO> list = new ArrayList<>();
		//EL CLIENTE DEBE DE EXISTIR EN LA TABLA CAPACIDADES Y BASICA DEL CLIENTE
		List<Map<String, Object>> clienteCapacidad = jdbcUtils.queryForList("SELECT_CLIENTE_CAPACIDAD", req.toMap());
		Map<String, Object> clienteBasica = jdbcUtils.queryForMap("SELECT_CLIENTE_BASICA", req.toMap());
		LOGGER.info("List ClienteCapa: {}",clienteCapacidad.size());
		if(!clienteCapacidad.isEmpty() && !clienteBasica.isEmpty()){
			//SE OBTIENE EL DECANTAMIENTO DE LOS PRODUCTOS DEL CLIENTE
			String decantamientoCliente = clienteBasica.get("ST_DECANTAMIENTO").toString();
			LOGGER.info("decantamientoCliente: {}",decantamientoCliente);
			Map<String, Object> infoDecantamiento = jdbcUtils.queryForMap("SELECT_INFO_DECANTA", decantamientoCliente);
			String autoDeca = infoDecantamiento.get("AUTO").toString();
			String nomDeca = infoDecantamiento.get("NOM").toString();
			String ppiDeca = infoDecantamiento.get("PPI").toString();
			String tdcDeca = infoDecantamiento.get("TDC").toString();
			String idDeca = infoDecantamiento.get("ID").toString();
			LOGGER.info("IDDECA: {}",idDeca);
			if(idDeca.equals("0")){
				//CAPACIDAD DE PAGO MAYOR A CERO DEL CLIENTE
				for(int i=0;i<clienteCapacidad.size();i++){
					Map<String, Object> capacidad = clienteCapacidad.get(i);
					LOGGER.info("IM_CAP_PAGO: {}",capacidad.get("IM_CAP_PAGO"));
					BigDecimal imCapPag = (BigDecimal) capacidad.get("IM_CAP_PAGO");
					BigDecimal cero = new BigDecimal(0);
					if(imCapPag.compareTo(cero)==1){
						LOGGER.info("Capacidad: {}",capacidad.get("CD_PRODUCTO"));
						
					}
				}
			}
		}
		
		
		
		
		//TP_CONSUMO(BASICO DEL CLIENTE) == CD_PRODUCTO(CAPACIADES)
		
		//SE GENERA SOLO EL PRODUCTO INFORMADO POR CAPACIDADES
		
		//**AUTO GENERA UNA ALTERNATIVA PARA MODELO NUEVO Y OTRA PARA SEMINUEVO
		
		//CALCULO DE LOS IMPORTES PARA LOS PRODUCTOS MENCIONADOS
		
		//IMPORTE ALTERNATIVA  --PLAZO PATA AUTO 60 Y TDC 24
		
		//IMPORTE DEL PAGO MENSUAL  **UNICAMENTE PARA PRODUCTOS DE CONSUMO Y AUTO
		//--IMPORTE ALTERNATIVA / NUMERO DE MESES(PLAZO)
		
		//REGLAS ADICIONALES
		
		//--PARA CONSUMO
		
		//COINCIDIR EL SEGMENTO COMERCIAL
		
		//INGRESO DEL CLIENTE DEBE ENCONTRARSE ENTRE EL MONTO MIN Y MAX DEL SUBPRODUCTO
		
		//IMPORTE DE ALTERNATIVA DEBE ENCONTRARSE ENTRE EL MONTO MIN Y MAX DEL SUBPRODUCTO
		
		//SI ES NOMINADO, TOMAR EN CUENTA A LA EMPRESA DISPERSORA.
		
		//EDAD DEL CLIENTE EN AÑOS Y MESES SE ENCUENTRE ENTRE LA EDAD MIN Y MAX DEL SUBPRODUCTO
		
		//SI LA CAPACIDAD DE PAGO EXCEDE AL SUBPRODUCTO RELACIONADO, CONSIDERAR EL MAX DEL SUBPRODUCTO EN LUGAR DE LA CAPACIDAD INFORMADA
		
		//EN CASO DE ENCONTRAR DOS SUBPRO QUE CUMPLAN LAS CASUISTICAS SE DEBE DE CONSIDERAR EL DE MEJOR PRIORIDAD
		
		//-- PARA TDC
		
		//DEBE DE CONSIDERARSE EL SEGMENTO COMERCIAL
		
		//CONSIDERAR LA EDAD MIN Y MAX
		
		//SI EL IMPORTE DE LA ALTERNATIVA CORRESPONDE A DOS PROCUSOS, ASIGNAR EL QUE TENGA MEJOR PRIORIDAD
		
		//--PARA AUTO
		
		//DEBE DE CONSIDERARSE EL SEGMENTO FIJO
		
		//**EXCEPCIONES PERMITIR
		
			//SEGMENTO COMERCIAL = P0 EL FIJO SERÁ "BANCA PRIVADA UHN"
			
			//SEGMENTO COMERCIAL = P1 EL FIJO SERÁ "BANCA PRIVADA"
		
			//SEGMENTO COMERCIAL = P2 EL FIJO SERÁ "BANCA PATRIMONIAL ALTO"
		
			//SEGMENTO COMERCIAL = P3 EL FIJO SERÁ "BANCA PATRIMONIAL BAJO"
		
			//OTRO, TOMARÁ EL SEGMENTO COMERCIAL INFORMADO
		
		//**EN TDC NO SE MANDA EL PLAZO
		//**EN CONSUMO SE TOMA EL PLAZO DE SUBPRODUCTO
		return list;
	}
	
}
