package com.bbva.reod;

import java.util.Arrays;
import java.util.Map;

import com.bbva.elara.domain.transaction.ParameterTable;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractREODT00201MXTransaction extends AbstractTransaction {

	public AbstractREODT00201MXTransaction(){
	}
	/**
	 * Return value for input parameter cliente
	 */
	protected String getCliente()
	{
		return (String)getParameter("cliente");
	}
	
	

	
	
	
	/**
	 * Set value for compound output parameter listCreditos
	 */
	protected void setListcreditos(final ParameterTable field)
	{
		this.addParameter("listCreditos", field);
	}
	
	private ParameterTable getOutputListcreditos()
	{
		return (ParameterTable)this.getParameter("listCreditos");
	}
	
	/**
	 * Insert a new row in compound output parameter listCreditos
	 */
	protected void setListcreditosRow(final Map<String, Object> rowToInsert)
	{
		ParameterTable param = (ParameterTable)this.getParameter("listCreditos");
		if( param == null )
		{		
			param = new ParameterTable(Arrays.asList(new String[] {"numcontrato", "cliente", "monto", "mensualidad", "producto", "fecha_contratacion"}));
		}
		param.add(rowToInsert);
		this.setListcreditos(param);
	}

}
