package com.bbva.reod;


import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractREODT00301MXTransaction extends AbstractTransaction {

	public AbstractREODT00301MXTransaction(){
	}
	/**
	 * Return value for input parameter contrato
	 */
	protected String getContrato()
	{
		return (String)getParameter("contrato");
	}
	
	

	
	/**
	 * Set value for output parameter status
	 */
	protected void setStatus(final String field)
	{
		this.addParameter("status", field);
	}
	

}
