package com.bbva.reod;

import java.util.Arrays;
import java.util.Map;

import com.bbva.elara.domain.transaction.ParameterTable;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractREODT00501MXTransaction extends AbstractTransaction {

	public AbstractREODT00501MXTransaction(){
	}
	/**
	 * Return value for input parameter cliente
	 */
	protected String getCliente()
	{
		return (String)getParameter("cliente");
	}
	
	

	
	
	
	/**
	 * Set value for compound output parameter listAlternativas
	 */
	protected void setListalternativas(final ParameterTable field)
	{
		this.addParameter("listAlternativas", field);
	}
	
	private ParameterTable getOutputListalternativas()
	{
		return (ParameterTable)this.getParameter("listAlternativas");
	}
	
	/**
	 * Insert a new row in compound output parameter listAlternativas
	 */
	protected void setListalternativasRow(final Map<String, Object> rowToInsert)
	{
		ParameterTable param = (ParameterTable)this.getParameter("listAlternativas");
		if( param == null )
		{		
			param = new ParameterTable(Arrays.asList(new String[] {"producto", "subproducto", "plazo", "tasa", "importeAlternativa", "pagoMensual", "descripcionSubproducto", "impMinimo", "impMaximo"}));
		}
		param.add(rowToInsert);
		this.setListalternativas(param);
	}

}
