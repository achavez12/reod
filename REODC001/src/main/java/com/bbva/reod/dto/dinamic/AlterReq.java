package com.bbva.reod.dto.dinamic;

import java.util.HashMap;
import java.util.Map;

import com.bbva.apx.dto.AbstractDTO;

public class AlterReq extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3119535690034851805L;
	
	private String cliente;
	private String producto;
	private String subproducto;
	private String impPersonalizado;
	public final String getCliente() {
		return cliente;
	}
	public final void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public final String getProducto() {
		return producto;
	}
	public final void setProducto(String producto) {
		this.producto = producto;
	}
	public final String getSubproducto() {
		return subproducto;
	}
	public final void setSubproducto(String subproducto) {
		this.subproducto = subproducto;
	}
	public final String getImpPersonalizado() {
		return impPersonalizado;
	}
	public final void setImpPersonalizado(String impPersonalizado) {
		this.impPersonalizado = impPersonalizado;
	}
	public static final long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((impPersonalizado == null) ? 0 : impPersonalizado.hashCode());
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + ((subproducto == null) ? 0 : subproducto.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlterReq other = (AlterReq) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (impPersonalizado == null) {
			if (other.impPersonalizado != null)
				return false;
		} else if (!impPersonalizado.equals(other.impPersonalizado))
			return false;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (subproducto == null) {
			if (other.subproducto != null)
				return false;
		} else if (!subproducto.equals(other.subproducto))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AlterReq [cliente=" + cliente + ", producto=" + producto + ", subproducto=" + subproducto
				+ ", impPersonalizado=" + impPersonalizado + "]";
	}
	
	public Map<String,Object> toMap() {
	    Map<String,Object> objectMap = new HashMap<>();
	    objectMap.put("cliente",this.cliente);
	    objectMap.put("producto",this.producto);
	    objectMap.put("subproducto",this.subproducto);
	    objectMap.put("impPersonalizado",this.impPersonalizado);
	    return objectMap;
	}

}
