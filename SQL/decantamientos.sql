--------------------------------------------------------
-- Archivo creado  - domingo-septiembre-08-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table DECANTAMIENTOS
--------------------------------------------------------

  CREATE TABLE "C##DINAMIC"."DECANTAMIENTOS" 
   (	"ID" NUMBER(2,0), 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"AUTO" VARCHAR2(1 BYTE), 
	"NOM" VARCHAR2(1 BYTE), 
	"PPI" VARCHAR2(1 BYTE), 
	"TDC" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into C##DINAMIC.DECANTAMIENTOS
SET DEFINE OFF;
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (0,'SIN DECANTAMIENTO','S','S','S','S');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (2,'INDESEABLE','N','N','N','N');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (3,'POSIBLE FRAUDE','N','N','N','N');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (5,'RFC SIN CALIDAD','S','S','S','S');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (6,'BONIFICACION UV','S','S','S','S');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (8,'EMPLEADOS BBVA','N','N','N','N');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (10,'EMPRESA','N','N','N','N');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (11,'INACTIVOS','N','N','N','N');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (12,'NO BANCA COMERCIAL','N','S','N','S');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (13,'CARTERA VENDIDA','N','N','N','N');
Insert into C##DINAMIC.DECANTAMIENTOS (ID,NOMBRE,AUTO,NOM,PPI,TDC) values (14,'MORAS','N','N','N','N');
