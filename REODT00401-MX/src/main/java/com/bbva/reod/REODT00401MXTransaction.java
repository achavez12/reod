package com.bbva.reod;

import java.nio.charset.Charset;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Advice;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.reod.dto.dinamic.CreditosResponse;
import com.bbva.reod.lib.r001.REODR001;

/**
 * Contrataci�n de una alternativa
 * Implementacion de logica de negocio.
 * @author MB87560
 *
 */
public class REODT00401MXTransaction extends AbstractREODT00401MXTransaction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(REODT00401MXTransaction.class);

	@Override
	public void execute() {
		REODR001 reodR001 = (REODR001)getServiceLibrary(REODR001.class);
		
		if(validaReq()){
			CreditosResponse req = new CreditosResponse();
			req.setCliente(getCliente());
			req.setFecha_contratacion(getFechaContratacion());
			req.setMensualidad(this.getMensualidad().toString());
			req.setMonto(this.getMonto().toString());
			req.setNumcontrato(getAlphaNumericString(18));
			req.setProducto(getProducto());
			
			String result = reodR001.executeContratacion(req);
			
			this.setStatus(result);
			
		}
		if (validaAdvice()) {
			return;
		}
	}
	
	 // function to generate a random string of length n 
    public String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }
	
	private boolean validaAdvice() {
		Advice myAdvice = getAdvice();
		if (myAdvice != null) {
			switch (myAdvice.getCode()) {
				// NO EXISTEN DATOS A LISTAR
			case "01319001":
				setSeverity(Severity.WARN);
				break;
			default:
				setSeverity(Severity.WARN);
			}
			return true;
		}
		return false;
	}
	
	private boolean validaReq(){
		boolean result=true;
		if(this.getCliente().isEmpty() && this.getFechaContratacion().isEmpty() &&
			this.getMensualidad().toString().isEmpty() && this.getMonto().toString().isEmpty() && 
			this.getProducto().isEmpty()){
			this.addAdvice("01319001");
			result=false;
			LOGGER.warn("ENTRY IS EMPTY.");
		}
		return result;
	}

}
