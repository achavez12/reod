package com.bbva.reod.lib.r001;

import java.util.List;

import com.bbva.reod.dto.dinamic.AlterReq;
import com.bbva.reod.dto.dinamic.AlternativasDTO;
import com.bbva.reod.dto.dinamic.CreditosResponse;
import com.bbva.reod.dto.dinamic.LoginReq;

public interface REODR001 {

	String executelogin(LoginReq req);
	
	List<CreditosResponse> executeCreditos(String client);
	
	String executeCancelarCredito(String contrato);
	
	String executeContratacion(CreditosResponse req);
	
	List<AlternativasDTO> executeAlternativas(AlterReq req);

}
