package com.bbva.reod;


import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractREODT00401MXTransaction extends AbstractTransaction {

	public AbstractREODT00401MXTransaction(){
	}
	/**
	 * Return value for input parameter cliente
	 */
	protected String getCliente()
	{
		return (String)getParameter("cliente");
	}
	/**
	 * Return value for input parameter monto
	 */
	protected Double getMonto()
	{
		return (Double)getParameter("monto");
	}
	/**
	 * Return value for input parameter mensualidad
	 */
	protected Double getMensualidad()
	{
		return (Double)getParameter("mensualidad");
	}
	/**
	 * Return value for input parameter producto
	 */
	protected String getProducto()
	{
		return (String)getParameter("producto");
	}
	/**
	 * Return value for input parameter fecha_contratacion
	 */
	protected String getFechaContratacion()
	{
		return (String)getParameter("fecha_contratacion");
	}
	
	

	
	/**
	 * Set value for output parameter status
	 */
	protected void setStatus(final String field)
	{
		this.addParameter("status", field);
	}
	

}
