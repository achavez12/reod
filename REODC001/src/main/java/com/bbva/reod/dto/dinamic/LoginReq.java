package com.bbva.reod.dto.dinamic;

import java.util.HashMap;
import java.util.Map;

import com.bbva.apx.dto.AbstractDTO;
/**
 * Clase Ejemplo para desarrollo de un DTO fuera del modelo canonico
 * @author beeva
 *
 */
public class LoginReq extends AbstractDTO {
	private static final long serialVersionUID = 2931699728946643245L;
	private String client;
	private String password;
	public final String getClient() {
		return client;
	}
	public final void setClient(String client) {
		this.client = client;
	}
	public final String getPassword() {
		return password;
	}
	public final void setPassword(String password) {
		this.password = password;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginReq other = (LoginReq) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LoginReq [client=" + client + ", password=" + password + "]";
	}

	public Map<String,Object> toMap() {
		Map<String,Object> objectMap = new HashMap<>();
		objectMap.put("client",this.client);
		objectMap.put("password",this.password);
		return objectMap;
	}


}
