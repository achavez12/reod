package com.bbva.reod;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Advice;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.reod.dto.dinamic.CreditosResponse;
import com.bbva.reod.lib.r001.REODR001;

/**
 * Consulta de creditos activos de un usuario
 * Implementacion de logica de negocio.
 * @author MB87560
 *
 */
public class REODT00201MXTransaction extends AbstractREODT00201MXTransaction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(REODT00201MXTransaction.class);
	private static final List<String> COLUMNS_RESPONSE = Arrays
			.asList("numcontrato", "cliente", "monto",
					"mensualidad", "producto", "fecha_contratacion");

	@Override
	public void execute() {
		REODR001 reodR001 = (REODR001)getServiceLibrary(REODR001.class);
		if(validaReq()){
			
			List<CreditosResponse> result = reodR001.executeCreditos(this.getCliente());
			
			addRowsToResponse(result);
		}
		if (validaAdvice()) {
			return;
		}
	}
	
	private boolean validaAdvice() {
		Advice myAdvice = getAdvice();
		if (myAdvice != null) {
			switch (myAdvice.getCode()) {
				// NO EXISTEN DATOS A LISTAR
			case "01319001":
				setSeverity(Severity.WARN);
				break;
			default:
				setSeverity(Severity.WARN);
			}
			return true;
		}
		return false;
	}
	
	private boolean validaReq(){
		boolean result=true;
		if(this.getCliente().isEmpty()){
			this.addAdvice("01319001");
			result=false;
			LOGGER.warn("ENTRY IS EMPTY.");
		}
		return result;
	}
	private void addRowsToResponse(List<CreditosResponse> creditos) {
		for (CreditosResponse credito : creditos) {
			Map<String,Object> response = new HashMap<>();
			Map<String, Object> toMap = credito.toMap();
			for(String key: COLUMNS_RESPONSE){
				LOGGER.warn("VAR= {}",toMap.get(key));
				response.put(key,toMap.get(key));
			}
			this.setListcreditosRow(response);
		}
	}

}
