package com.bbva.reod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Advice;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.reod.dto.dinamic.LoginReq;
import com.bbva.reod.dto.dinamic.LoginReqBuilder;
import com.bbva.reod.lib.r001.REODR001;

/**
 * Proceso de Login
 * Implementacion de logica de negocio.
 * @author MB87560
 *
 */
public class REODT00101MXTransaction extends AbstractREODT00101MXTransaction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(REODT00101MXTransaction.class);

	@Override
	public void execute() {
		REODR001 reodR001 = (REODR001)getServiceLibrary(REODR001.class);
		
		if(validaReq()){
			LoginReq req = buildRequest();
			
			String result = reodR001.executelogin(req);
			
			this.setStatus(result);
		}
		if (validaAdvice()) {
			return;
		}
	}
	
	private LoginReq buildRequest() {
		LoginReqBuilder builder = LoginReqBuilder.aLoginReqBuilder();
		builder.withClient(this.getNumcliente());
		builder.withPassword(this.getPassword());		
		return builder.build();
	}

	private boolean validaAdvice() {
		Advice myAdvice = getAdvice();
		if (myAdvice != null) {
			switch (myAdvice.getCode()) {
				// NO EXISTEN DATOS A LISTAR
			case "01319001":
				setSeverity(Severity.WARN);
				break;
			default:
				setSeverity(Severity.WARN);
			}
			return true;
		}
		return false;
	}
	
	private boolean validaReq(){
		boolean result=true;
		if(this.getNumcliente().isEmpty() || this.getPassword().isEmpty()){
			this.addAdvice("01319001");
			result=false;
			LOGGER.warn("ENTRY IS EMPTY.");
		}
		return result;
	}

}
