package com.bbva.reod.dto.dinamic;

import com.bbva.apx.dto.AbstractDTO;

public class LoginReqBuilder extends AbstractDTO {
	private static final long serialVersionUID = -4634875534947535819L;
	private String client;
    private String password;
    
    public static LoginReqBuilder aLoginReqBuilder() {
        return new LoginReqBuilder();
      }

	public LoginReqBuilder withClient(final String client) {
		this.client = client;
		return this;
	}

	public LoginReqBuilder withPassword(final String password) {
		this.password = password;
		return this;
	}
    
	public LoginReq build() {
		LoginReq loginreq = new LoginReq();
		loginreq.setClient(client);
		loginreq.setPassword(password);
	    return loginreq;
	  }
    
}
