package com.bbva.reod;
import com.bbva.reod.lib.r001.REODR001;

/**
 * Cancelaci�n logica de creditos de un usuario
 * Implementacion de logica de negocio.
 * @author MB87560
 *
 */
public class REODT00301MXTransaction extends AbstractREODT00301MXTransaction {

	@Override
	public void execute() {
		REODR001 reodR001 = (REODR001)getServiceLibrary(REODR001.class);
		
		String result = reodR001.executeCancelarCredito(this.getContrato());
		
		this.setStatus(result);
	}

}
