package com.bbva.reod.dto.dinamic;

import java.util.HashMap;
import java.util.Map;

import com.bbva.apx.dto.AbstractDTO;

public class CreditosResponse extends AbstractDTO{

	private static final long serialVersionUID = 7089031792396490947L;
	private String numcontrato;
    private String cliente;
    private String monto;
    private String mensualidad;
    private String producto;
    private String fecha_contratacion;
    private String status;

    

	public final String getNumcontrato() {
		return numcontrato;
	}



	public final void setNumcontrato(String numcontrato) {
		this.numcontrato = numcontrato;
	}



	public final String getCliente() {
		return cliente;
	}



	public final void setCliente(String cliente) {
		this.cliente = cliente;
	}



	public final String getMonto() {
		return monto;
	}



	public final void setMonto(String monto) {
		this.monto = monto;
	}



	public final String getMensualidad() {
		return mensualidad;
	}



	public final void setMensualidad(String mensualidad) {
		this.mensualidad = mensualidad;
	}



	public final String getProducto() {
		return producto;
	}



	public final void setProducto(String producto) {
		this.producto = producto;
	}



	public final String getFecha_contratacion() {
		return fecha_contratacion;
	}



	public final void setFecha_contratacion(String fecha_contratacion) {
		this.fecha_contratacion = fecha_contratacion;
	}



	public final String getStatus() {
		return status;
	}



	public final void setStatus(String status) {
		this.status = status;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((fecha_contratacion == null) ? 0 : fecha_contratacion.hashCode());
		result = prime * result + ((mensualidad == null) ? 0 : mensualidad.hashCode());
		result = prime * result + ((monto == null) ? 0 : monto.hashCode());
		result = prime * result + ((numcontrato == null) ? 0 : numcontrato.hashCode());
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreditosResponse other = (CreditosResponse) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (fecha_contratacion == null) {
			if (other.fecha_contratacion != null)
				return false;
		} else if (!fecha_contratacion.equals(other.fecha_contratacion))
			return false;
		if (mensualidad == null) {
			if (other.mensualidad != null)
				return false;
		} else if (!mensualidad.equals(other.mensualidad))
			return false;
		if (monto == null) {
			if (other.monto != null)
				return false;
		} else if (!monto.equals(other.monto))
			return false;
		if (numcontrato == null) {
			if (other.numcontrato != null)
				return false;
		} else if (!numcontrato.equals(other.numcontrato))
			return false;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	

	@Override
	public String toString() {
		return "CreditosResponse [numcontrato=" + numcontrato + ", cliente=" + cliente + ", monto=" + monto
				+ ", mensualidad=" + mensualidad + ", producto=" + producto + ", fecha_contratacion="
				+ fecha_contratacion + ", status=" + status + "]";
	}



	public Map<String,Object> toMap() {
	    Map<String,Object> objectMap = new HashMap<>();
	    objectMap.put("numcontrato",this.numcontrato);
	    objectMap.put("cliente",this.cliente);
	    objectMap.put("monto",this.monto);
	    objectMap.put("mensualidad",this.mensualidad);
	    objectMap.put("producto",this.producto);
	    objectMap.put("fecha_contratacion",this.fecha_contratacion);
	    objectMap.put("status",this.status);
	    return objectMap;
	  }
    
}
