package com.bbva.reod;
import java.util.List;
import java.util.Map;

import com.bbva.reod.dto.dinamic.AlterReq;
import com.bbva.reod.dto.dinamic.AlternativasDTO;
import com.bbva.reod.lib.r001.REODR001;

/**
 * Generaci�n de alternativas
 * Implementacion de logica de negocio.
 * @author MB87560
 *
 */
public class REODT00501MXTransaction extends AbstractREODT00501MXTransaction {

	@Override
	public void execute() {
		REODR001 reodR001 = (REODR001)getServiceLibrary(REODR001.class);
		
		AlterReq req = new AlterReq();
		req.setCliente(getCliente());
		
		List<AlternativasDTO> list = reodR001.executeAlternativas(req);
		
	}

}
