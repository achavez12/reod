package com.bbva.reod.dto.dinamic;

import com.bbva.apx.dto.AbstractDTO;

public class CreditosResponseBuilder extends AbstractDTO{
	
	private static final long serialVersionUID = -3754290505604217302L;
	private String numcontrato;
    private String cliente;
    private String monto;
    private String mensualidad;
    private String producto;
    private String fecha_contratacion;
    private String status;
    
    public static CreditosResponseBuilder aCreditosResponseBuilder() {
        return new CreditosResponseBuilder();
      }
    
	public CreditosResponseBuilder withNumContrato(String numcontrato) {
		this.numcontrato = numcontrato;
		return this;
	}
	public CreditosResponseBuilder withCliente(String cliente) {
		this.cliente = cliente;
		return this;
	}
	public CreditosResponseBuilder withMonto(String monto) {
		this.monto = monto;
		return this;
	}
	public CreditosResponseBuilder withMensualidad(String mensualidad) {
		this.mensualidad = mensualidad;
		return this;
	}
	public CreditosResponseBuilder withProducto(String producto) {
		this.producto = producto;
		return this;
	}
	public CreditosResponseBuilder withFecha_contratacion(String fecha_contratacion) {
		this.fecha_contratacion = fecha_contratacion;
		return this;
	}
	public CreditosResponseBuilder withStatus(String status) {
		this.status = status;
		return this;
	}
    
	public CreditosResponse build() {
		CreditosResponse loginreq = new CreditosResponse();
		loginreq.setCliente(cliente);
		loginreq.setFecha_contratacion(fecha_contratacion);
		loginreq.setNumcontrato(numcontrato);
		loginreq.setMensualidad(mensualidad);
		loginreq.setMonto(monto);
		loginreq.setProducto(producto);
		loginreq.setStatus(status);
	    return loginreq;
	  }
    

}
