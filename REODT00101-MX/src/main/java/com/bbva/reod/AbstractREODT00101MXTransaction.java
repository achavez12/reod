package com.bbva.reod;


import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractREODT00101MXTransaction extends AbstractTransaction {

	public AbstractREODT00101MXTransaction(){
	}
	/**
	 * Return value for input parameter numcliente
	 */
	protected String getNumcliente()
	{
		return (String)getParameter("numcliente");
	}
	/**
	 * Return value for input parameter password
	 */
	protected String getPassword()
	{
		return (String)getParameter("password");
	}
	
	

	
	/**
	 * Set value for output parameter status
	 */
	protected void setStatus(final String field)
	{
		this.addParameter("status", field);
	}
	

}
